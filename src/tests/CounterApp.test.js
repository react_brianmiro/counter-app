import { shallow } from 'enzyme';
import CounterApp from '../CounterApp';

describe('Testing on CounterApp component', () => {

   let wrapper ;
    beforeEach( () => {

         wrapper = shallow(
            <CounterApp value = '10' />
        );

    });

    test('Create snapshot for CounterApp', () => {

        expect(wrapper).toMatchSnapshot();
    });

    test('should be to show the value 10', () => {

        const value = '10';
        const wrapper = shallow(
            <CounterApp
                value={value}
            />
        );

        const valueByDefect = wrapper.find('h2').text().trim();
        expect(valueByDefect).toBe(value);

    });

    test('should be increment with add button +1', () => {

        wrapper.find('button').at(0).simulate('click');

       const counterText = wrapper.find('h2').text().trim();

       expect( counterText ).toBe('101');

    });

    test('should be decrement  with add button -1', () => {

        wrapper.find('button').at(2).simulate('click');

       const counterText = wrapper.find('h2').text().trim();

       expect( counterText ).toBe('9');

    });

    test('should be reset value  with  reset  button ', () => {

        const wrapper = shallow(
            <CounterApp
                value={ '105' }
            />
        );
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(0).simulate('click');

        wrapper.find('button').at(1).simulate('click');

       const counterText = wrapper.find('h2').text().trim();

       expect( counterText ).toBe('105');

    });


})


