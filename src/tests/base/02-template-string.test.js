import '@testing-library/jest-dom';
import { getSaludo} from '../../base/02-template-string.js';


describe('Pruebas en 02-template-string', () => {

    test('getSaludo debe retornar hola fernando', () => {
        
        const nombre = 'Brian';

        const saludo = getSaludo( nombre );

        expect( saludo ).toBe('Hola ' + nombre );
    });

    test('getSaludo debe retornar Hola Carlos si no hay argumento', () => {
        
        const saludo = getSaludo(  );
        
        expect( saludo ).toBe('Hola Carlos' );

    });
    
    
});