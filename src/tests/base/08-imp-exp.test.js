import { getHeroeById,getHeroesByOwner } from "../../base/08-imp-exp";
import heroes from "../../data/heroes";


describe('Pruebas en funciones de Heroes', () => {

    test('debe retornar un heroe por id', () => {

        const id = 7;
        const heroe = getHeroeById( id );
        
        const heroeData = heroes.find( h => h.id === id );

        expect( heroe ).toEqual( heroeData );

    });

    test('debe retornar undefined si Heroe no existe', () => {

        const id = 10;
        const heroe = getHeroeById( id );

        expect( heroe ).toBe( undefined );

    });

    test('debe retornar un arreglo con los heroes de DC', () => {

        const owner = 'DC';
        const heroe = getHeroesByOwner( owner);

        const heroeData = heroes.filter( (heroe) => heroe.owner === owner );

        expect( heroe ).toEqual( heroeData );

    });

    test('debe retornar un arreglo con los heroes de DC', () => {

        const owner = 'DC';
        const heroe = getHeroesByOwner( owner);

        const heroeData = heroes.filter( (h) => h.owner === owner );

        expect( heroe ).toEqual( heroeData );

    });

    test('debe retornar un arreglo con los heroes de Marvel', () => {

        const owner = 'Marvel';
        const heroes = getHeroesByOwner( owner);

        expect( heroes.length ).toBe( 2 );

    });


});